import path from "path";
import { fileURLToPath } from "url";
import { packageDirectorySync } from "pkg-dir";
const cwd = process.cwd();

// esm 模块没有 CommonJS的 __dirname
// 这里需要通过工具函数fileURLToPath来封装一个 __dirname
// 注意：这里的 __dirname 指向的是当前文件 utils/index.js 的路径，因此加了一层 ../ 来将路径修改到 bin目录
export const __dirname = fileURLToPath(import.meta.url);

/**
 * 本项目绝对路径
 */
export const thisProjectRoot = packageDirectorySync({ cwd: __dirname });

/**
 * 本项目中文件的路径
 */
export const getThisProjectFilePath = (...filePath) => {
  return path.resolve(thisProjectRoot, ...filePath);
};

/**
 * 业务侧项目根目录
 */
export const businessProjectRoot = packageDirectorySync({ cwd });

/**
 * 业务侧项目根目录
 */
export const getBusinessProjectFilePath = (...filePath) =>
  path.join(businessProjectRoot, ...filePath);
