import { execa, execaSync } from "execa";
import { businessProjectRoot } from "./index.js";

// 处理公共配置
const commonConfig = (_cmd, _args, _options) => {
  let options = {
    cwd: businessProjectRoot,
    stdio: "pipe",
    ..._options,
  };
  const args = _args || [];

  return {
    cmd: _cmd,
    args,
    options,
  };
};

/**
 * 异步处理命令
 * @param _cmd
 * @param _args
 * @param _options
 * @returns {ExecaChildProcess}
 */
export const runCmd = (_cmd, _args, _options = {}) => {
  const { cmd, args, options } = commonConfig(_cmd, _args, _options);
  return execa(cmd, args, options);
};

/**
 * 同步处理命令
 * @param _cmd
 * @param _args
 * @param _options
 * @returns {ExecaSyncReturnValue}
 */
export const runCmdSync = (_cmd, _args, _options = {}) => {
  const { cmd, args, options } = commonConfig(_cmd, _args, _options);
  return execaSync(cmd, args, options);
};
