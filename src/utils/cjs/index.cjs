const path = require("path");

/**
 * 业务侧项目根目录 cjs
 */
const getBusinessProjectFilePathCjs = (...filePath) => {
  return path.join(process.cwd(), ...filePath);
};

module.exports = {
  getBusinessProjectFilePathCjs,
};
