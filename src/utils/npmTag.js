import minimist from "minimist";

export const argv = minimist(process.argv.slice(3));

/**
 * 获取 npm 命令参数
 * @returns {null|string[]|*}
 */
export const getNpmArgs = () => {
  // console.log(`npm_command`, process.env.npm_command)
  // console.log(process.env)
  if (process.env.npm_command) {
    return [process.env.npm_command];
  }

  let npmArgv = null;

  // console.log(`npm_config_argv`, process.env.npm_config_argv)
  try {
    npmArgv = JSON.parse(process.env.npm_config_argv);
  } catch (err) {
    return null;
  }

  if (
    typeof npmArgv !== "object" ||
    !npmArgv.cooked ||
    !Array.isArray(npmArgv.cooked)
  ) {
    return null;
  }

  return npmArgv.cooked;
};

/**
 * 获取当前 npm tag
 * @returns {string}
 */
export const getNpmTag = () => {
  let tag = "";
  if (argv["npm-tag"]) {
    tag = argv["npm-tag"];
  }
  return tag;
};

/**
 * 判断当前 npm tag 是否是正式版本
 * @returns {boolean}
 */
export const npmTagIsRelease = () => {
  return !getNpmTag()?.length;
};

/**
 * 判断当前 npm tag 是否是 alpha
 * @returns {boolean}
 */
export const npmTagIsAlpha = () => getNpmTag() === "alpha";

/**
 * 判断当前 npm tag 是否是 beta
 * @returns {boolean}
 */
export const npmTagIsBeta = () => {
  return getNpmTag() === "beta";
};
