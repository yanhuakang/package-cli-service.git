export default {
  input: "src/index.js",
  outputDir: "dist",
  esDir: "es",
  cjsDir: "lib",
  umdDir: "umd",
  storybook: {
    outputDir: "dist-storybook",
  },
};
