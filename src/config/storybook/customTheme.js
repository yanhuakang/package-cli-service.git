import { create } from "@storybook/theming/create";

let defConfig = {
  brandTitle: "DEMO",
  brandUrl: "",
  ...(process.env.PROJECT_STORY_CONFIG || {}),
};

export default create(defConfig);
