const { getBusinessProjectFilePathCjs } = require("../../utils/cjs/index.cjs");

const defStores = [
  getBusinessProjectFilePathCjs("src/**/*.doc.mdx"),
  getBusinessProjectFilePathCjs("src/**/*.stories.mdx"),
  getBusinessProjectFilePathCjs("src/**/*.stories.@(js|jsx|ts|tsx)"),
  getBusinessProjectFilePathCjs("src/**/*.doc.@(js|jsx|ts|tsx)"),
];
let storybook_config = {
  brandTitle: require(getBusinessProjectFilePathCjs("package.json")).name,
  brandUrl: "",
};
module.exports = {
  stories: defStores,
  addons: [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-interactions",
    "storybook-preset-less",
  ],
  framework: "@storybook/react",
  env: function (config) {
    return {
      ...config,
      PROJECT_STORY_CONFIG: storybook_config,
    };
  },
};
