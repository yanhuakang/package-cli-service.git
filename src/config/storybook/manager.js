import { addons } from "@storybook/addons";
import theme from "./customTheme.js";

addons.setConfig({
  isFullscreen: false,
  showNav: true,
  showPanel: true,
  backgrounds: false,
  panelPosition: "bottom",
  sidebarAnimations: true,
  enableShortcuts: false,
  isToolshown: true,
  theme: theme,
  selectedPanel: undefined,
  initialActive: "sidebar",
  showRoots: true,
  ...(process.env.PROJECT_STORY_MANAGER || {}),
});
