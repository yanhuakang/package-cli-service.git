import { buildStatic } from "@storybook/core-server";
import {
  getBusinessProjectFilePath,
  getThisProjectFilePath,
} from "../utils/index.js";
import getPkgToolsConfig, {
  getPackageJson,
} from "../utils/getPkgToolsConfig.js";
import { createRequire } from "module";

/**
 * 为 example 启动开发服务器
 */

const require = createRequire(import.meta.url);
const pathName = require.resolve("@storybook/react/dist/cjs/server/preset");

const packageJson = getPackageJson();
const { storybookConfig = {} } = await getPkgToolsConfig();

buildStatic({
  packageJson: packageJson,
  framework: "react",
  frameworkPresets: [pathName],
  configDir: storybookConfig.configDir
    ? getBusinessProjectFilePath(storybookConfig.configDir)
    : getThisProjectFilePath("src/config/storybook"),
  outputDir: getBusinessProjectFilePath(storybookConfig.outputDir),
});
