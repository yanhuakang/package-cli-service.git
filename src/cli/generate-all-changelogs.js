import { runCmdSync } from "../utils/runCmd.js";
import { getThisProjectFilePath } from "../utils/index.js";
import ora from "ora";

/**
 * 生成所有更新日志
 */
const generateAllChangelogs = () => {
  const generateSpinner = ora("开始发布...");
  generateSpinner.start();
  console.log(" ");
  const configFilePath = getThisProjectFilePath(
    "src/config/changelog-option.cjs"
  );
  console.log(
    `conventional-changelog -p custom-config -i CHANGELOG.md -s -n ${configFilePath} -r 0`
  );
  try {
    runCmdSync("conventional-changelog", [
      "-p",
      "custom-config",
      "-i",
      "CHANGELOG.md",
      "-s",
      "-n",
      configFilePath,
      "-r",
      0,
    ]);
    generateSpinner.succeed("生成成功！");
  } catch (err) {
    generateSpinner.succeed("生成失败！");
  }
};
export default generateAllChangelogs;
