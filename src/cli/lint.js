import ora from "ora";
import { runCmd } from "../utils/runCmd.js";
import chalk from "chalk";

/**
 * 使用 eslint 检测代码中的问题
 * @param argv                命令行参数
 * @returns {Promise<void>}
 */
export default async (argv = { _: [] }) => {
  const range = argv._.length ? argv._ : ["./src", "./example"];
  const buildSpinner = ora("开始检测...");
  buildSpinner.start();
  runCmd("npx", ["eslint", ...range], { stdio: "inherit" })
    .then(() => {
      buildSpinner.succeed("检测完成");
    })
    .catch(() => {
      buildSpinner.succeed("检测行完毕，以请修复以上问题");
      console.log(
        `你可以执行 ${chalk.hex("#ffd700")("npm run fix")} 修复以上问题`
      );
    });
};
