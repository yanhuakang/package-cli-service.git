import ora from "ora";
import { runCmd } from "../utils/runCmd.js";

/**
 * 自动解决 eslint 问题
 * @param argv              命令行参数
 * @returns {Promise<void>}
 */
export default async (argv = { _: [] }) => {
  const range = argv._.length ? argv._ : ["./src", "./example"];
  const buildSpinner = ora("开始修复...");
  buildSpinner.start();
  runCmd("npx", ["eslint", ...range, "--fix"], {
    stdio: "inherit",
  })
    .then(() => {
      buildSpinner.succeed("修复完成");
    })
    .catch(() => {
      buildSpinner.succeed("执行完毕，以上问题需手动修复");
    });
};
