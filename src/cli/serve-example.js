import { runCmd } from "../utils/runCmd.js";

/**
 * 为 example 启动开发服务器
 * @param argv        命令行参数
 * @param options     options
 */
export default (argv, options) => {
  runCmd("npx", ["webpack-dev-server"], {
    // stdio: [2, 2, 2],
    stdio: "inherit",
  });
};
