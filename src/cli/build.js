import build from "../core/build.js";

/**
 * 编译打包组件
 * @param argv
 * @param options
 * @returns {Promise<{failed: boolean}>}
 */
export default async (argv, options) => {
  await build(argv, options);
};
