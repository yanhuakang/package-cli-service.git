import build from "../core/build.js";

/**
 * 代码分析
 * @param argv              命令行参数
 * @param options           options
 * @returns {Promise<void>}
 */
const analyzer = async (argv, options) => {
  process.env.package_cli_service_analyzer = "true";
  await build(argv, options);
};
export default analyzer;
