import ora from "ora";
import { runCmdSync } from "../utils/runCmd.js";
import build from "../core/build.js";

/**
 * 对 example 进行生产环境编译
 * @param argv                命令行参数
 * @param options             options
 * @returns {Promise<void>}
 */
export default async (argv, options) => {
  const buildComponentsSpinner = ora("开始编译组件...");
  buildComponentsSpinner.start();
  console.log(" ");
  const result = await build(argv, options);
  if (result.failed) {
    buildComponentsSpinner.fail("组件编译失败！");
  } else {
    buildComponentsSpinner.succeed("组件编译成功！");
  }

  const buildExampleSpinner = ora("开始编译 example...");
  buildExampleSpinner.start();
  console.log(" ");

  try {
    runCmdSync("npx", ["webpack", "--env", "prod", "--env", "min"], {
      // stdio: [2, 2, 2],
      stdio: "inherit",
    });
    buildExampleSpinner.succeed("example 编译成功！");
  } catch (err) {
    console.error(err);
    buildExampleSpinner.fail("example 编译失败！");
  }
};
