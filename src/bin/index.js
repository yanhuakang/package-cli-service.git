#!/usr/bin/env node

import { program } from "commander";
import chalk from "chalk";
import leven from "leven";
import minimist from "minimist";
// 增强常见错误消息
import enhanceErrorMessages from "../utils/enhanceErrorMessages.js";
import { getPackageJson } from "../utils/getPkgToolsConfig.js";

const packageJson = getPackageJson();

program
  .name("package-cli-service")
  .usage("<command> [options]")
  .description(
    "package-cli-service 用来构建、打包、发布 package，并集成了很多服务于 package 的功能，如 serve、storybook、changelog等功能"
  )
  .version(packageJson.version);

program
  .command("pub")
  .description("发布 package")
  .option("-a, --npm-tag=alpha", "发布 alpha 版本")
  .option("-b, --npm-tag=beta", "发布 beta 版本")
  .option("-c, --config", "自定义配置文件")
  .option("--skip-git-status", "跳过git status 检测")
  .option("--skip-fetch-tags", "跳过拉取tags")
  .option("--custom-version", "自定义版本号")
  .option("--skip-compile", "跳过编译")
  .option("--skip-append-changelog", "跳过向 README.md 写入更新日志")
  .option("--skip-publish", "跳过发布")
  .option("--skip-robot", "跳过机器人消息推送")
  .action(async (options) => {
    const argv = minimist(process.argv.slice(3));
    console.log("process.argv", process.argv);
    console.log("process.argv.slice(3)", process.argv.slice(3));
    console.log("minimist(process.argv.slice(3))", argv);
    if (argv._.length > 0) {
      console.log(
        chalk.yellow(
          `\n 警告：您提供了多余的参数。${argv._.join("、")} 将被忽略。`
        )
      );
    }
    import("../cli/pub.js").then(({ default: pub }) => {
      pub(argv, options);
    });
  });

program
  .command("serve-example")
  .description("为 example 启动开发服务器")
  .action(async (options) => {
    const argv = minimist(process.argv.slice(3));
    import("../cli/serve-example.js").then(({ default: serveExample }) => {
      serveExample(argv, options);
    });
  });

program
  .command("build-example")
  .description("对 example 进行生产环境编译")
  .action(async (options) => {
    const argv = minimist(process.argv.slice(3));
    import("../cli/build-example.js").then(({ default: buildExample }) => {
      buildExample(argv, options);
    });
  });

program
  .command("lint")
  .description("使用 eslint 检测代码中的问题")
  .action(async () => {
    const argv = minimist(process.argv.slice(3));
    import("../cli/lint.js").then(({ default: lint }) => {
      lint(argv);
    });
  });

program
  .command("fix")
  .description("自动解决 eslint 问题")
  .action(async () => {
    const argv = minimist(process.argv.slice(3));
    import("../cli/fix.js").then(({ default: fix }) => {
      fix(argv);
    });
  });

program
  .command("analyzer")
  .description("可视化并分析package，以查看哪些模块占用了空间")
  .action(async (options) => {
    const argv = minimist(process.argv.slice(3));
    import("../cli/analyzer.js").then(({ default: analyzer }) => {
      analyzer(argv, options);
    });
  });

program
  .command("build")
  .option("-c, --config", "自定义配置文件")
  .option("-w, --watch", "查看bundle中的文件并根据更改重新构建")
  .description("编译打包 package")
  .action(async (options) => {
    const argv = minimist(process.argv.slice(3));
    console.log("process.argv", process.argv);
    console.log("process.argv.slice(3)", process.argv.slice(3));
    console.log("minimist(process.argv.slice(3))", argv);
    if (argv._.length > 0) {
      console.log(
        chalk.yellow(
          `\n 警告：您提供了多余的参数。${argv._.join("、")} 将被忽略。`
        )
      );
    }
    import("../cli/build.js").then(({ default: build }) => {
      build(argv, options);
    });
  });

// https://storybook.js.org/docs/react/api/cli-options#start-storybook
program
  .command("serve-storybook")
  .description("为 storybook 启动开发服务器")
  .option("-p, --port", "端口")
  .action(() => {
    import("../cli/serve-storybook.js");
  });

// https://storybook.js.org/docs/react/api/cli-options#build-storybook
program
  .command("build-storybook")
  .description("对 storybook 进行生产环境编译")
  .action(() => {
    import("../cli/build-storybook.js");
  });

program
  .command("prepublish")
  .description("组件包使用 npm publish 之前的拦截器")
  .action(() => {
    import("../cli/prepublish.js").then(({ default: prepublish }) => {
      prepublish();
    });
  });

program
  .command("generate-all-changelogs")
  .description("生成所有更新日志")
  .action(() => {
    import("../cli/generate-all-changelogs.js").then(
      ({ default: generateAllChangelogs }) => {
        generateAllChangelogs();
      }
    );
  });

// output help information on unknown commands
program.on("command:*", ([cmd]) => {
  program.outputHelp();
  console.log(`  ` + chalk.red(`Unknown command ${chalk.yellow(cmd)}.`));
  console.log();
  suggestCommands(cmd);
  process.exitCode = 1;
});

program.commands.forEach((c) => c.on("--help", () => console.log()));

// 命令缺少必填参数
enhanceErrorMessages("missingArgument", (argName) => {
  return `Missing required argument ${chalk.yellow(`<${argName}>`)}.`;
});

// 未知 option
enhanceErrorMessages("unknownOption", (optionName) => {
  return `Unknown option ${chalk.yellow(optionName)}.`;
});

// option 缺少必填参数
enhanceErrorMessages("optionMissingArgument", (option, flag) => {
  return (
    `Missing required argument for option ${chalk.yellow(option.flags)}` +
    (flag ? `, got ${chalk.yellow(flag)}` : ``)
  );
});

function suggestCommands(unknownCommand) {
  const availableCommands = program.commands.map((cmd) => cmd._name);

  let suggestion;

  availableCommands.forEach((cmd) => {
    const isBestMatch =
      leven(cmd, unknownCommand) < leven(suggestion || "", unknownCommand);
    if (leven(cmd, unknownCommand) < 3 && isBestMatch) {
      suggestion = cmd;
    }
  });

  if (suggestion) {
    console.log(`  ` + chalk.red(`Did you mean ${chalk.yellow(suggestion)}?`));
  }
}

program.parse();
