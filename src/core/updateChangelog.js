import fs from "fs";
import { runCmdSync } from "../utils/runCmd.js";
import {
  getBusinessProjectFilePath,
  getThisProjectFilePath,
} from "../utils/index.js";
import { getNpmTag, argv } from "../utils/npmTag.js";
import editChangelogQuestions from "../questions/editChangelogQuestions.js";

/**
 * 增加 git tag、更新 CHANGELOG.md
 * @returns {Promise<void>}
 */
const updateChangelog = async () => {
  const configFilePath = getThisProjectFilePath(
    "src/config/changelog-option.cjs"
  );
  console.log(
    `conventional-changelog -p custom-config -i CHANGELOG.md -s -n ${configFilePath}`
  );
  runCmdSync("conventional-changelog", [
    "-p",
    "custom-config",
    "-i",
    "CHANGELOG.md",
    "-s",
    "-n",
    configFilePath,
  ]);

  // 是否手动修改 changelog
  await editChangelogQuestions();

  let current_version = JSON.parse(
    fs.readFileSync(getBusinessProjectFilePath("package.json"), "utf-8")
  ).version;
  console.log(`current_version`, current_version);
  // if (npmTagIsAlpha()) {
  //   console.log(`当前发布TAG:${getNpmTag()}，跳过提交 version 操作`);
  //   return;
  // }

  console.log(`当前发布TAG:${getNpmTag()}，提交 version 操作`);

  console.log("执行 git add --all");
  runCmdSync("git", ["add", "--all"]);

  console.log(`执行 git commit -am "build: pub ${current_version}"`);
  runCmdSync("git", ["commit", "-am", `build: pub ${current_version}`]);

  console.log(`执行 git tag ${current_version}`);
  runCmdSync("git", ["tag", current_version]);
};
export default updateChangelog;
