import { runCmdSync, runCmd } from "../utils/runCmd.js";

/**
 * 发布失败，回退
 * 1. 删除生成的git tag
 * 2. 回退 commit
 */
const revert = () => {
  // 1. 删除生成的git tag
  const { stdout: currentTag } = runCmdSync(
    "git",
    ["describe", "--abbrev=0", "--tags"],
    { stdio: "pipe" }
  );
  runCmd("git", ["tag", "-d", currentTag]);

  // 2. 回退 commit
  const { stdout } = runCmdSync("git", ["rev-list", "--max-count=2", `--all`], {
    stdio: "pipe",
  });
  const prevCommitId = stdout.split("\n")[1];
  runCmdSync("git", ["reset", "--keep", prevCommitId]);
};
export default revert;
