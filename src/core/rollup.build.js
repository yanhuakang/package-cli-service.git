import { runCmd } from "../utils/runCmd.js";

/**
 * rollup -c 打包
 * @param configFilePath
 * @param cwd
 * @param rollupArgv
 * @returns {ExecaChildProcess}
 */
export default (configFilePath, cwd, rollupArgv) =>
  runCmd("rollup", ["-c", configFilePath, ...rollupArgv], {
    cwd: cwd,
    stdio: "inherit",
  });
