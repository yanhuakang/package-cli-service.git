import inquirer from "inquirer";

export default () =>
  inquirer.prompt([
    {
      type: "list",
      name: "releaseTag",
      message: "请选择发布类型",
      choices: [
        { value: "major", label: "major" },
        { value: "minor", label: "minor" },
        { value: "patch", label: "patch" },
      ],
      default: "patch",
    },
  ]);
