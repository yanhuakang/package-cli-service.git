import inquirer from "inquirer";

export default () =>
  inquirer.prompt([
    {
      type: "expand",
      name: "whetherEditChangelog",
      message: `已更新 CHANGELOG.md，当前可手动修改 CHANGELOG.md。回车继续进程`,
      default: "Yes",
      choices: [{ key: "Y", value: "Yes" }],
    },
  ]);
