# package-cli-service



## 介绍

CLI 服务 (`package-cli-service`) 是一个开发环境依赖。它是一个 npm 包，局部安装在每个 `create-package-tools` 创建的项目中。

CLI 服务是构建于[rollup](https://rollupjs.org/)、 [webpack](http://webpack.js.org/) 和 [webpack-dev-server](https://github.com/webpack/webpack-dev-server) 之上的。它包含了：

- 一个针对绝大部分 package 优化过的内部的 rollup 配置；
- 项目内部的 `package-cli-service` 命令，提供 `serve-example`、`build-example`、`lint`、`fix`、`analyzer`、`build` 、`pub` 、`serve-storybook`、`build-storybook` 命令。

如果你熟悉 [@vue/cli-service](https://cli.vuejs.org/zh/guide/cli-service.html) 的话，`package-cli-service` 实际上大致等价于 `@vue/cli-service`，尽管功能集合不一样。但`package-cli-service` 专注于为 package 提供完整的工作流。



<br />



## 使用命令

`package-cli-service` 安装了一个名为 `package-cli-service` 的命令。你可以在 npm scripts 中以 `package-cli-service`、或者从终端中以 `./node_modules/.bin/package-cli-service` 访问这个命令。

这是你使用默认 preset 的项目的 `package.json`：

```json
"scripts": {
  "serve-example": "package-cli-service serve-example",
  "build-example": "package-cli-service build-example",
  "lint": "package-cli-service lint",
  "fix": "package-cli-service fix",
  "analyzer": "package-cli-service analyzer",
  "build": "package-cli-service build",
  "pub": "package-cli-service pub",
  "alpha": "package-cli-service pub --npm-tag=alpha",
  "beta": "package-cli-service pub --npm-tag=beta",
  "prepublishOnly": "package-cli-service prepublish",
  "serve-storybook": "package-cli-service serve-storybook -p 6006",
  "build-storybook": "package-cli-service build-storybook",
  "all-changelogs": "package-cli-service generate-all-changelogs"
},
```

你可以通过 npm 或 Yarn 调用这些 script：

```bash
npm run pub
# OR
yarn pub
```

如果你可以使用 [npx](https://github.com/npm/npx) (最新版的 npm 应该已经自带)，也可以直接这样调用命令：

```bash
npx package-cli-service pub
```



<br />



### package-cli-service pub

```
用法：package-cli-service serve

选项：

  --npm-tag=alpha   				发布 alpha 版本
  --npm-tag=beta    				发布 beta 版本
  --config   				   	自定义配置文件
  --skip-git-status				跳过git status 检测
  --skip-compile   				跳过编译
  --skip-append-changelog		        跳过向 README.md 写入更新日志
  --skip-robot   				跳过机器人消息推送
  --skip-publish    				跳过发布
  --custom-version  				自定义版本号
  --skip-fetch-tags 				跳过拉取 git tags
```

`package-cli-service pub` 命令会发布组件。它是 `package-cli-service ` 最重要最核心的功能，该命令集成了以下几个工作流：

> **package-cli-service pub 工作流**
>
> 1. 更新 version
> 2. 自动生成changelog
> 3. 编译打包
> 4. 发布
> 5. 机器人消息推送



<br />



### package-cli-service serve-example

```
用法：package-cli-service serve-example
```

`package-cli-service serve-example` 命令会为 `example` 启动一个开发服务器 (基于 [webpack-dev-server](https://github.com/webpack/webpack-dev-server)) 并附带开箱即用的模块热重载 (Hot-Module-Replacement)。



<br />



### package-cli-service build-example

```
用法：package-cli-service build-example
```

`package-cli-service build-example` 命令会打包 `example` 项目



<br />



### package-cli-service lint

```
用法：package-cli-service lint
```

`package-cli-service lint` 命令会使用 `eslint` 默认检测 `src`、`example` 代码中的问题。

你也可以自定义你要检测的文件，只需要这样做 `package-cli-service lint ./components1 ./components2`



<br />



### package-cli-service fix

```
用法：package-cli-service fix
```

`package-cli-service fix` 命令会使用 `eslint` 默认修复 `src`、`example` 代码中的问题。

你也可以自定义你要检测的文件，只需要这样做 `package-cli-service fix ./components1 ./components2`



<br />



### package-cli-service analyzer

```
用法：package-cli-service analyzer
```

`package-cli-service analyzer` 命令会使用 `rollup-plugin-visualizer` 来可视化并分析组件包，以查看哪些模块占用了空间。



<br />



### package-cli-service build

```
用法：package-cli-service build

选项：

  --config      自定义配置文件
  --watch       查看bundle中的文件并根据更改重新构建
```

`package-cli-service build` 命令只会编译打包组件，不进行发布



<br />



### package-cli-service serve-storybook

```
用法：package-cli-service serve-storybook

选项：

  --port      	指定 storybook 运行的端口
```

`package-cli-service serve-storybook` 命令会启动 `storybook`，提供可视化组件及组件文档



<br />



### package-cli-service build-storybook

```
用法：package-cli-service build-storybook
```

`package-cli-service build-storybook` 命令会编译打包storybook，输出一个静态资源`dist-storybook`，你可以将之发布在你的组件在线文档中



<br />



### package-cli-service prepublish

```
用法：package-cli-service prepublish
```

`package-cli-service prepublish` 命令是组件包发布前的的拦截器，该命令会**阻止**非使用 `package-cli-service` 进行发布组件包。

如使用 `npm publish` 将会被阻止。



<br />



### generate-all-changelogs

```
用法：package-cli-service generate-all-changelogs
```

`package-cli-service generate-all-changelogs` 命令是给组件包生成所有更新日志。



<br />



### 查看所有的可用命令

你可以运行以下命令查看所有命令：

```bash
npx package-cli-service help
```

你也可以这样学习每个命令可用的选项：

```bash
npx package-cli-service help [command]
```



<br />



## 配置参考 package-cli-service.config.js

### input

- 包的入口点

- Type: `string | string []| { [entryName: string]: string }`
- Default: `src/index.js`



### outputDir

- 定义输出 package 所创建的 *bundle* 的目录名

- Type: `string`
- Default: `dist`



### cjsDir

- 定义输出 commonjs 所创建的 *bundle* 的目录名

- Type: `string`
- Default: `lib`



### esDir

- 定义输出 ES module 所创建的 *bundle* 的目录名

- Type: `string`
- Default: `es`



### umdDir

- 定义输出 umd 所创建的 *bundle* 的目录名

- Type: `string`
- Default: `umd`



### output.umd

- 是否输出 umd 格式的 *bundle* 

- Type: `boolean`
- Default: `false`



### output.name

- 对于iife/umd包来说是必需的，在这种情况下，它是代表你的包的全局变量名

- Type: `string`
- Default: `none`



### output.globals

- 指定umd/iife包中外部导入所需的 `id: variableName` 对
- Type: `{ [id: string]: string }| ((id: string) => string)`
- Default: `none`
- 

### output.plugins

- 该选项用于指定输出插件，这是设置插件的唯一入口
- Type: `OutputPlugin | (OutputPlugin | void)[]`



### external

- 从输出的 bundle 中排除依赖，**防止**将某些 `import` 的包(package)**打包**到 bundle 中，而是在运行时(runtime)再去从外部获取这些*扩展依赖(external dependencies)*。

- Type: `(string | RegExp)[]| RegExp| string| (id: string, parentId: string, isResolved: boolean) => boolean`
- Default: `none`



### eslintOptions

- [@rollup/plugin-eslint](https://www.npmjs.com/package/@rollup/plugin-eslint) 的options。一个Rollup插件，它用于lint入口点和所有ESLint导入的文件

- Type: `object`



### externalsOptions

- [rollup-plugin-node-externals](https://www.npmjs.com/package/rollup-plugin-node-externals) 的options。一个Rollup插件，自动将NodeJS内置模块声明为外部模块。也处理npm dependencies, devDependencies, peerDependencies and optionalDependencies

- Type: `object`



### aliasOptions

- [@rollup/plugin-alias](https://www.npmjs.com/package/@rollup/plugin-alias) 的options。一个Rollup插件，创建 `import` 或 `require` 的别名，来确保模块引入变得更简单

- Type: `object`

- Default:

  - entries:  `@ => src`

  - customResolver: 

    ```js
    resolve({
      extensions: ['.js', '.ts', '.jsx', '.tsx', '.less', '.scss', '.mjs']
    })
    ```



### resolveOptions

- [@rollup/plugin-node-resolve](https://www.npmjs.com/package/@rollup/plugin-node-resolve) 的options。一个Rollup插件，可以让 Rollup 查找到外部模块

- Type: `object`
- Default:
  - extensions: `['.js', '.ts', '.jsx', '.tsx', '.less', '.scss', '.mjs']`



### commonjsOptions

- [@rollup/plugin-commonjs](https://www.npmjs.com/package/@rollup/plugin-commonjs) 的options。一个Rollup插件，目前，NPM上的大多数包都输出为CommonJS模块。`@rollup/plugin-commonjs` 可以在Rollup处理CommonJS之前将它们转换为ES2015

- Type: `object`



### urlOptions

- [@rollup/plugin-url](https://www.npmjs.com/package/@rollup/plugin-url) 的options。一个Rollup插件，可以导入JPG, PNG, GIF, SVG和WebP文件。

- Type: `object`

- Default: 

  ```js
  {
    fileName: '[dirname][name][extname]',
    sourceDir: path.join(__dirname, 'src')
  }
  ```



### typescriptOptions

- [rollup-plugin-typescript2](https://www.npmjs.com/package/rollup-plugin-typescript2) 的options。带有编译器错误的typescript Rollup插件

- Type: `object`



### babelOptions

- [@rollup/plugin-babel](https://www.npmjs.com/package/@rollup/plugin-babel) 的options。在rollup里应用 babel 解析ES6的桥梁

- Type: `object`

- Default:

  ```js
  {
    extensions: [
      ...DEFAULT_EXTENSIONS,
      ".ts",
      ".tsx",
    ],
    babelHelpers: "runtime",
    exclude: "node_modules/**",
    presets: [
      [
        "@babel/preset-env",
        {
          modules: false,
          useBuiltIns: "usage",
          corejs: 3,
        },
      ],
      "@babel/preset-react",
      "@babel/preset-typescript"
    ],
    plugins: ["@babel/plugin-transform-runtime"],
  }
  ```



### postcssOptions

- [rollup-plugin-postcss](https://www.npmjs.com/package/rollup-plugin-postcss) 的options。Rollup和PostCSS之间的无缝集成。

- Type: `object`

- Default:

  ```js
  {
    // extract: true,
    // minimize: true,
    ...postcssOptions,
    plugins: [
      autoprefixer(),
      postcssUrl({
        url: "inline", // enable inline assets using base64 encoding
        maxSize: 100, // maximum file size to inline (in kilobytes)
        fallback: "copy",
        includeUriFragment: true, // 在 URI 末尾包含片段标识符
        ignoreFragmentWarning: true, // 内联带有片段的 SVG URL 时不发出警告
        optimizeSvgEncode: true, // 减小内联 svg 的大小（IE9+，Android 3+）
      }),
    ],
    use: {
      less: {
        plugins: [new npmImport({ prefix: '~' })], // 使用带～前缀引入less文件
        javascriptEnabled: true
      },
    }
  }
  ```



### stripOptions

- [@rollup/plugin-strip](https://www.npmjs.com/package/@rollup/plugin-strip) 的options。一个Rollup插件，在打包时从代码中删除 `debugger`、`assert.equal` 和 `console.*`

- Type: `object`

- Default:

  ```js
  {
    labels: ["unittest"],
  }
  ```



### terserOptions

- [@rollup/plugin-terser](https://www.npmjs.com/package/@rollup/plugin-terser) 的options。一个Rollup插件，进行代码压缩

- Type: `object`



### deleteOptions

- [rollup-plugin-delete](https://www.npmjs.com/package/rollup-plugin-delete) 的options。一个Rollup插件，当你想在打包之前清理dist或其他文件夹和文件时，这个插件是有用的

- Type: `object`

- Default:

- ```js
  {
    targets: `${outputDir}/*`,
  }
  ```

  

### visualizerOptions

- [rollup-plugin-visualizer](https://www.npmjs.com/package/rollup-plugin-visualizer) 的options。一个Rollup插件，可视化并分析Rollup包，以查看哪些模块占用了空间。

- Type: `object`

- Default:

  ```js
  {
    filename: `./dist-visualizer/stats.html`,
    title: `${packageJson.name} visualizer`,
    open: true,
    template: "treemap",
    gzipSize: true,
  }
  ```




### copyOptions

- [rollup-plugin-copy](https://www.npmjs.com/package/rollup-plugin-copy) 的options。一个Rollup插件，打包时复制文件和文件夹
- Type: `object`



### copyToDistExt

- 需要在打包时复制到outputDir目录的文件后缀
- Type: `string[]`
- Default: `['.less']`



### storybookConfig

- storybook 配置
- Type: `object`
  - configDir: 
    - .storybook 目录地址
    - Type: `relative path`
  - outputDir: 
    - build-storybook 输出目录
    - Type: `string`
    - Default: `dist-storybook`
  - robotDetailUrl: 
    - 详情地址
    - Type: `string`
    - Default: `https://www.npmjs.com/package/${packageName}`



### robotConfig

- package 发布|删除|废弃 时推送飞书机器人消息
- Type: `object`
  - robotUrl: 
    - 推送地址
    - Type: `string`
  - packageName: 
    - 包名
    - Type: `string`
    - Default: `package.json name`
  - robotDetailUrl: 
    - 详情地址
    - Type: `string`
    - Default: `https://www.npmjs.com/package/${packageName}`





