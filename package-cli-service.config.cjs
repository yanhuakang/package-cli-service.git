const path = require("path");
const packageJson = require("./package.json");
module.exports = () => {
  return {
    outputDir: 'src',
    robotConfig: {
      // TODO 设置你的机器人 webhook 地址：https://www.feishu.cn/hc/zh-CN/articles/410063847664
      robotUrl: ''
    },
  };
};
